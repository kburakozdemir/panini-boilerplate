# Panini Starter Template

> This one is for Bootstrap.

To be used for rapid prototyping. Edit the project to fit your needs.

## Pipeline Status

[![pipeline status](https://gitlab.com/kburakozdemir/panini-boilerplate/badges/master/pipeline.svg)](https://gitlab.com/kburakozdemir/panini-boilerplate/commits/master)

## References

1. [Panini GitHub](https://github.com/foundation/panini)
2. [Modularizing Your Bootstrap Template With Zurb Panini](https://www.brav3.net/blog/coding/modularizing-your-bootstrap-template-with-zurb-panini/)
3. [How to Make Gulp Copy a Directory AND Its Contents](https://opnsrce.github.io/how-to-make-gulp-copy-a-directory-and-its-contents)
4. [Gulp for Beginners](https://css-tricks.com/gulp-for-beginners/)
5. [Gulp Sass with autoprefixer and minify.](https://gist.github.com/leymannx/f7867942184d01aa2311)
6. [Just Sharing My Gulpfile](https://css-tricks.com/just-sharing-my-gulpfile/)

## Versions

| Software | Version |
| -------- | ------- |
| node     | 14.14.0 |
| npm      | 6.14.8  |

## Usage

Run `npm ci` after cloning repo.

### Troubleshooting

If `npm ci` does not work, run `npm install`.

### `npm` Commands

`watch` -> `npm run watch`

`build` -> `npm run build`

After build, you can deploy the created `public` folder.

### Edit

Only edit files in `app` folder.

## GitLab Pages

After GitLab CI/CD pipeline job finishes, visit [this URL](https://kburakozdemir.gitlab.io/panini-boilerplate/).
