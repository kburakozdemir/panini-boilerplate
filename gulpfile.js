"use strict";

// Load plugins
const gulp = require("gulp");
const clean = require("gulp-clean");
const sass = require("gulp-sass");
sass.compiler = require("node-sass");
const prefix = require("gulp-autoprefixer");
const browsersync = require("browser-sync").create();
const panini = require("panini");

// folders
const folder = {
  src: "app/",
  build: "public/",
};

// BrowserSync
function browserSync(done) {
  browsersync.init({
    server: "./public",
    port: 3000,
  });
  done();
}

// BrowserSync Reload
function browserSyncReload(done) {
  browsersync.reload();
  done();
}

function cleandist() {
  return gulp
    .src("public", {
      read: false,
      allowEmpty: true,
    })
    .pipe(clean());
}

// // In order to add folders from node_modules
// // uncomment gulp.src array entries or add folders there.
function copyvendor() {
  return gulp
    .src(
      [
        ".",
        // 'node_modules/glightbox/**/*'
      ],
      {
        base: "./node_modules/",
        allowEmpty: true,
      }
    )
    .pipe(gulp.dest("public/vendor"));
}

function copyjs() {
  return gulp
    .src(["app/js/**/*"], {
      base: "./app/",
    })
    .pipe(gulp.dest("public"));
}

function copycss() {
  return gulp
    .src("app/scss/styles.scss")
    .pipe(sass())
    .pipe(
      prefix({
        cascade: false,
      })
    )
    .pipe(gulp.dest("public/css"));
}

function copyassets() {
  return gulp
    .src(["app/assets/**/*"], {
      base: "./app/",
    })
    .pipe(gulp.dest("public"));
}

function html() {
  var url = "html/",
    htmlRoot = folder.src + url,
    out = folder.build;
  return gulp
    .src(htmlRoot + "pages/" + "**/*.html")
    .pipe(
      panini({
        root: htmlRoot + "pages/",
        layouts: htmlRoot + "layouts/",
        partials: htmlRoot + "partials/",
        helpers: htmlRoot + "helpers/",
        data: htmlRoot + "data/",
      })
    )
    .pipe(gulp.dest(out));
}

const resetPages = (done) => {
  panini.refresh();
  done();
};

// Watch files
function watchFiles() {
  gulp.watch(
    [
      folder.src + "html/{pages,layouts,partials,helpers,data}/**/*.html",
      folder.src + "{js,scss}/**/*.*",
    ],
    gulp.series(
      resetPages,
      cleandist,
      copyvendor,
      copyjs,
      copycss,
      copyassets,
      html,
      browserSyncReload
    )
  );
}

// define complex tasks
const build = gulp.series(
  resetPages,
  cleandist,
  copyvendor,
  copyjs,
  copycss,
  copyassets,
  html
);
const watch = gulp.series(build, gulp.parallel(watchFiles, browserSync));

// export tasks
exports.build = build;
exports.watch = watch;
